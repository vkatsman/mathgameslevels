import {addRuleToCollectionIfNotExists} from "./mechanics/helpers.js";

export class Level
{
    static levels = [];
    static attendantRules = [
        {left:"A&(!A|B)",right:"A&B"},
        {left:"A|(B|C)",right:"A|B|C"},
        {left:"A|B|C",right:"A|(B|C)"},
        {left:"A&(B&C)",right:"A&B&C"},
        {left:"A&B&C",right:"A&(B&C)"},
        {left:"A&(B&C)",right:"(A&B)&C"},
        {left:"(A|B)|C",right:"A|B|C"},
        {left:"A|B|C",right:"(A|B)|C"},
        {left:"(A&B)&C",right:"A&B&C"},
        {left:"A&B&C",right:"(A&B)&C"},
        {left:"A|(B&C)",right:"(A|B)&(A|C)"},
        {left:"(A|B)&(A|C)",right:"A|(B&C)"},
        {left:"A&(B|C)",right:"(A&B)|(A&C)"},
        {left:"(A&B)|(A&C)",right:"A&(B|C)"},
        {left:"!(A&B)",right:"!A|!B"},
        {left:"!(A&B)",right:"!(B&A)"},
        {left:"!A|!B",right:"!(A&B)"},
        {left:"A|B",right:"!(!A&!B)"},
        {left:"A&B",right:"!(!A|!B)"},
        {left:"!(A|B)",right:"!A&!B"},
        {left:"!(A|B)",right:"!B&!A"},
        {left:"!(A|B)",right:"!(B|A)"},
        {left:"!A&!B",right:"!(A|B)"},
        {left:"!A|B",right:"A->B"},
        {left:"A->B",right:"!A|B"},
        {left:"A&!B",right:"A\\B"},
        {left:"A\\B",right:"A&!B"},
        {left:"!(!A)",right:"A"},
        {left:"A&(A|B)",right:"A"},
        {left:"A|A",right:"A"},
        {left:"A&A",right:"A"},
        {left:"A&B",right:"B&A"},
        {left:"A|B",right:"B|A"},
        {left:"A\\B",right:"!(B->A)"},
        {left:"!(B->A)",right:"A\\B"},
        {left:"B->A",right:"!(A\\B)"},
        {left:"!(A\\B)",right:"B->A"}
    ];

    static currentIndex = 0;
    static getIndexOfLast = () => Level.levels.length - 1;
    static next = () => Level.currentIndex++;

    constructor(
        taskId,
        name,
        steps,
        scope,
        startingExpression,
        goalExpression,
        hearts,
        time,
        optimalRules,
        attendantRules
    )
    {
        Object.assign(this, {
            taskId,
            name,
            steps,
            scope,
            startingExpression,
            goalExpression,
            hearts,
            time,
            optimalRules,
            attendantRules
        });

        Level.levels.push(this);

        Level.attendantRules.forEach(rule => {
            // [rule, {left: rule.right, right: rule.left}].forEach(rule => {
            //     addRuleToCollectionIfNotExists(rule, this.attendantRules)
            // });
            addRuleToCollectionIfNotExists(rule, this.attendantRules);
        });
    }
}

// ADDING LEVELS
const level0 = new Level(100,
    "Level 0 Tutorial",
    1,
    "setTheory",
    "A&(A|B)",
    "A",
    2,
    100,
    [
        {
            left: "A&(A|B)",
            right: "A"
        }
    ],
    [
        {
            left: "A&(A|B)",
            right: "A"
        }
    ]
);

const level1 = new Level(101,
    "Level 1 Simple",
    6,
    "setTheory",
    "A\\(B|C)",
    "(A\\B)\\C",
    4,
    280,
    [
        {
            left: "A\\B",
            right: "A&!B"
        },
        {
            left: "!(A|B)",
            right: "!A&!B"
        },
        {
            left: "A&(B&C)",
            right: "(A&B)&C"
        },
        {
            left: "A&!B",
            right: "A\\B"
        }],
    [
    ]
);

const level4 = new Level(104,
    "Level 2 Simple",
    5,
    "setTheory",
    "(A\\B)\\C",
    "A\\(B|C)",
    3,
    280,
    [
        {
            left: "A\\B",
            right: "A&!B"
        },
        {
            left: "A\\B",
            right: "A&!B"
        },
        {
            left: "(A&B)&C",
            right: "A&(B&C)"
        },
        {
            left: "!(A&B)",
            right: "!A|!B"
        },
        {
            left: "A&!B",
            right: "A\\B"
        }        ],
    [
    ]
);

const level5 = new Level(105,
    "Level 3 Simple",
    5,
    "setTheory",
    "A\\(B\\C)",
    "(A\\B)|(A&C)",
    6,
    280,
    [
        {
            left: "A\\B",
            right: "A&!B"
        },
        {
            left: "!(A&B)",
            right: "!A|!B"
        },
        {
            left: "!(!A)",
            right: "A"
        },
        {
            left: "A&B&C",
            right: "A&(B&C)"
        },
        {
            left: "A\\B",
            right: "A&!B"
        }        ],
    [
    ]
);

const level6 = new Level(106,
    "Level 4 Simple",
    6,
    "setTheory",
    "A\\(B|C)",
    "!(B->A)\\C",
    6,
    280,
    [
        {
            left: "A\\B",
            right: "A&!B"
        },
        {
            left: "!(A&B)",
            right: "!A|!B"
        },
        {
            left: "A&(B&C)",
            right: "(A&B)&C"
        },
        {
            left: "A\\B",
            right: "A&!B"
        }      ],
    [
    ]
);

const level7 = new Level(107,
    "Level 5 Simple",
    3,
    "setTheory",
    "A|(B\\A)",
    "A|B",
    3,
    280,
    [
        {
            left: "A\\B",
            right: "A&!B"
        },
        {
            left: "A|B&C",
            right: "(A&B)|(A&C)"
        },
        {
            left: "(A|B)&(A|!A)",
            right: "A|B"
        }      ],
    [
    ]
);

const level11 = new Level(111,
    "Level 6 Simple",
    3,
    "setTheory",
    "(A&B)|(A\\B)",
    "A",
    3,
    280,
    [
        {
            left: "A\\B",
            right: "!A&B"
        },
        {
            left: "A|B&A|C",
            right: "A|(B&C)"
        },
        {
            left: "A&(B|!B)",
            right: "A"
        }  ],
    [
    ]
);

const level12 = new Level(112,
    "Level 7 Simple",
    4,
    "setTheory",
    "!A&!B&!C",
    "!(A|B|C)",
    4,
    280,
    [
        {
            left: "A&B&C",
            right: "A&(B&C)"
        },
        {
            left: "(!A&!B)",
            right: "!(A|B)"
        },
        {
            left: "(!A&!B)",
            right: "!(A|B)"
        },
        {
            left: "A&(B&C)",
            right: "A&B&C"
        }  ],
    [
    ]
);

const level13 = new Level(113,
    "Level 8 Simple",
    2,
    "setTheory",
    "!A&!B&!(C->B)",
    "!(A|B)&!(!C|B)",
    3,
    280,
    [
        {
            left: "A->B",
            right: "!A|B"
        },
        {
            left: "A&B&C",
            right: "(A&B)&C"
        }	 ],
    [
    ]
);

const level20 = new Level(120,
    "Level 9 Simple",
    3,
    "setTheory",
    "!(!A|B)",
    "A\\B",
    3,
    250,
    [
        {
            left: "!(A|B)",
            right: "!A&!B"
        },
        {
            left: "!(!A)",
            right: "A"
        },
        {
            left: "A&!B",
            right: "A\\B"
        }        	 ],
    [
    ]
);

const level18 = new Level(118,
    "Level 10 Simple",
    3,
    "setTheory",
    "(!A\\B)|C",
    "A|B->C",
    3,
    250,
    [
        {
            left: "A\\B",
            right: "A&!B"
        },
        {
            left: "!A&!B",
            right: "!(A|B)"
        },
        {
            left: "!A|B",
            right: "A->B"
        }         	 ],
    [
    ]
);


const level3 = new Level(103,
    "Level 11 Middle",
    6,
    "setTheory",
    "A\\(A\\B)",
    "A&B",
    6,
    300,
    [
        {
            left: "A\\B",
            right: "A&!B"
        },
        {
            left: "!(A&B)",
            right: "!A|!B"
        },
        {
            left: "!(!A)",
            right: "A"
        },
        {
            left: "A&(B|C)",
            right: "A&B|A&C"
        },
        {
            left: "(A&!A)|(A&B)",
            right: "A&B"
        }        ],
    [
    ]
);

const level8 = new Level(108,
    "Level 12 Middle",
    6,
    "setTheory",
    "!(A&!B)|B",
    "A->B",
    6,
    300,
    [
        {
            left: "!(A&B)",
            right: "!A|!B"
        },
        {
            left: "!(!A)",
            right: "A"
        },
        {
            left: "(A|B)|C",
            right: "A|(B|C)"
        },
        {
            left: "B|B",
            right: "B"
        },
        {
            left: "!A|B",
            right: "A->B"
        }   ],
    [
    ]
);

const level9 = new Level(109,
    "Level 13 Middle",
    5,
    "setTheory",
    "!(A&(B\\C))",
    "!A|!B|C",
    5,
    300,
    [
        {
            left: "!(A&B)",
            right: "!A|!B"
        },
        {
            left: "A\\B",
            right: "!A&B"
        },
        {
            left: "!(!A)",
            right: "A"
        },
        {
            left: "A&(B&C)",
            right: "A&B&C"
        }  ],
    [
    ]
);

const level10 = new Level(110,
    "Level 14 Middle",
    4,
    "setTheory",
    "(!A&!B)->C",
    "A|B|C",
    3,
    130,
    [
        {
            left: "A->B",
            right: "!A|B"
        },
        {
            left: "(!A&!B)",
            right: "!(A|B)"
        },
        {
            left: "!(!A)",
            right: "A"
        }  ],
    [
    ]
);

const level14 = new Level(114,
    "Level 15 Difficult",
    7,
    "setTheory",
    "(A&B)->(!C|B)",
    "A->(B->(C->B))",
    6,
    300,
    [
        {
            left: "A->B",
            right: "!A|B"
        },
        {
            left: "!(A&B)",
            right: "!A|!B"
        },
        {
            left: "(A|B)|C",
            right: "A|B|C"
        },
        {
            left: "!A|B",
            right: "A->B"
        },
        {
            left: "A|B|C",
            right: "A|(B|C)"
        },
        {
            left: "!A|B",
            right: "A->B"
        }        	 ],
    [
    ]
);

const level2 = new Level(102,
    "Level 16 Difficult",
    6,
    "setTheory",
    "(A\\B)\\(B\\C)",
    "A&!B",
    6,
    350,
    [
        {
            left: "A&(B|C)",
            right: "(A&B)|(A&C)"
        },
        {
            left: "(A&B)&C",
            right: "A&(B&C)"
        },
        {
            left: "A&(B&C)",
            right: "(A&B)&C"
        },
        {
            left: "A&(A|B)",
            right: "A"
        },
        {
            left: "A&A",
            right: "A"
        }    ],
    [
    ]
);

const level16 = new Level(116,
    "Level 17 Difficult",
    5,
    "setTheory",
    "(A->!B)->C",
    "(A&B)|C",
    5,
    150,
    [
        {
            left: "A->!B",
            right: "!A|!B"
        },
        {
            left: "A->B",
            right: "!A|B"
        },
        {
            left: "!(A|B)",
            right: "!A&!B"
        },
        {
            left: "!(!A)",
            right: "A"
        }         	 ],
    [
    ]
);

const level17 = new Level(117,
    "Level 18 Difficult",
    5,
    "setTheory",
    "!A&(!B\\C)",
    "!((!A->B)|C)",
    5,
    450,
    [
        {
            left: "A->B",
            right: "!A|B"
        },
        {
            left: "!(!A)",
            right: "A"
        }        	 ],
    [
    ]
);

const level15 = new Level(115,
    "Level 19 Most Difficult",
    9,
    "setTheory",
    "((A\\B)\\C)|(!B|C)",
    "(A->(B|C))->(B->C)",
    9,
    400,
    [
        {
            left: "!A|B",
            right: "A->B"
        },
        {
            left: "A\\B",
            right: "A&!B"
        },
        {
            left: "(A|B)|C",
            right: "A|(B|C)"
        },
        {
            left: "!A&!B",
            right: "!(A|B)"
        },
        {
            left: "!A|B",
            right: "A->B"
        }      	 ],
    [
    ]
);

const level19 = new Level(119,
    "Level 20 Most Difficult",
    7,
    "setTheory",
    "!((!A&!B)|(!C&!D))",
    "(A|B)&(C|D)",
    2,
    280,
    [
        {
            left: "!(A|B)",
            right: "!A&!B"
        },
        {
            left: "!(A|B)",
            right: "!A&!B"
        },
        {
            left: "!(!A)",
            right: "A"
        }       	 ],
    [
    ]
);
