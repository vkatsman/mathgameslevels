import {addRuleToCollectionIfNotExists} from "./mechanics/helpers.js";

export class Level
{
    static levels = [];
    static attendantRules = [];

    static currentIndex = 0;
    static getIndexOfLast = () => Level.levels.length - 1;
    static next = () => Level.currentIndex++;

    constructor(
        steps,
        scope,
        startingExpression,
        goalExpression,
        hearts,
        time,
        optimalRules,
        attendantRules
    )
    {
        Object.assign(this, {
            steps,
            scope,
            startingExpression,
            goalExpression,
            hearts,
            time,
            optimalRules,
            attendantRules
        });

        Level.levels.push(this);

         attendantRules.forEach(rule => {
             [rule, {left: rule.right, right: rule.left}].forEach(rule => {
                 addRuleToCollectionIfNotExists(rule, this.attendantRules)
             });
             // addRuleToCollectionIfNotExists(rule, Level.attendantRules);
         });
    }
}

// ADDING LEVELS
const level0 = new Level(
    2,
    "",
    "C(k,n)",
    "A(k,n)/k!",
    2,
    60,
    [
        {
            left: "C(k,n)",
            right: "k!/(n!(m-n)!)"
        },
        {
            left: "k!/(n!(m-n)!)",
            right: "C(k,n)"
        },
        {
            left: "A(k,n)",
            right: "k!/(m-n)!"
        },
        {
            left: "k!/(m-n)!",
            right: "A(k,n)"
        },
        {
            left: "P(n)",
            right: "n!"
        },
        {
            left: "n!",
            right: "P(n)"
        }
    ],
    // [
    //     {
    //         left: "!(a&b)",
    //         right: "!a&!b"
    //     },
    //     {
    //         left: "!a&!b",
    //         right: "!(a&b)"
    //     }
    // ],
    [
        
    ]
);

const level1 = new Level(
    2,
    "",
    "A(n-1,n)",
    "P(n)",
    2,
    60,
    [
        {
            left: "C(k,n)",
            right: "k!/(n!(m-n)!)"
        },
        {
            left: "k!/(n!(m-n)!)",
            right: "C(k,n)"
        },
        {
            left: "A(k,n)",
            right: "k!/(m-n)!"
        },
        {
            left: "k!/(m-n)!",
            right: "A(k,n)"
        },
        {
            left: "P(n)",
            right: "n!"
        },
        {
            left: "n!",
            right: "P(n)"
        }
    ],
    // [
    //     {
    //         left: "!(a&b)",
    //         right: "!a&!b"
    //     },
    //     {
    //         left: "!a&!b",
    //         right: "!(a&b)"
    //     }
    // ],
    [
        
    ]
);

const level2 = new Level(
    2,
    "",
    "A(n-1,n)",
    "P(n)",
    2,
    60,
    [
        {
            left: "C(k,n)",
            right: "k!/(n!(m-n)!)"
        },
        {
            left: "k!/(n!(m-n)!)",
            right: "C(k,n)"
        },
        {
            left: "A(k,n)",
            right: "k!/(m-n)!"
        },
        {
            left: "k!/(m-n)!",
            right: "A(k,n)"
        },
        {
            left: "P(n)",
            right: "n!"
        },
        {
            left: "n!",
            right: "P(n)"
        }
    ],
    // [
    //     {
    //         left: "!(a&b)",
    //         right: "!a&!b"
    //     },
    //     {
    //         left: "!a&!b",
    //         right: "!(a&b)"
    //     }
    // ],
    [
        
    ]
);

const level3 = new Level(
    2,
    "",
    "C(m,m-n)",
    "C(m,n)",
    2,
    60,
    [
        {
            left: "C(m,(m-n))",
            right: "m!/((m-n)!(m-(m-n))!)"
        },
        {
            left: "m!/((m-n)!(m-(m-n))!)",
            right: "C(m,(m-n))"
        },
        {
            left: "a-(b-c)",
            right: "a-b+c"
        },
        {
            left: "a-b+c",
            right: "a-(b-c)"
        },
        {
            left: "a-b-c",
            right: "(a-b)-c"
        },
        {
            left: "(a-b)-c",
            right: "a-b-c"
        },
        {
            left: "(a-b)-c",
            right: "a-b-c"
        },
        {
            left: "(a-b)-c",
            right: "a-b-c"
        },
        {
            left: "a-a+b",
            right: "b"
        },
        {
            left: "C(k,n)",
            right: "k!/(n!(m-n)!)"
        },
        {
            left: "k!/(n!(m-n)!)",
            right: "C(k,n)"
        },
        {
            left: "A(k,n)",
            right: "k!/(m-n)!"
        },
        {
            left: "k!/(m-n)!",
            right: "A(k,n)"
        },
        {
            left: "P(n)",
            right: "n!"
        },
        {
            left: "n!",
            right: "P(n)"
        }
    ],
    // [
    //     {
    //         left: "!(a&b)",
    //         right: "!a&!b"
    //     },
    //     {
    //         left: "!a&!b",
    //         right: "!(a&b)"
    //     }
    // ],
    [
        
    ]
);

const level4 = new Level(
    2,
    "",
    "C(m-1,n)+C(m-1,n-1)",
    "C(m,n)",
    2,
    60,
    [
        {
            left:"C(m,(m-n))",
            right:"m!/((m-n)!(m-(m-n))!)"
        },
        {
            left:"m!/((m-n)!(m-(m-n))!)",
            right:"C(m,(m-n))"
        },
        {
            left:"C(m,n)",
            right:"m!/(n!(m-n)!)"
        },
        {
            left:"m!/(n!(m-n)!)",
            right:"C(m,n)"
        },
        {
            left:"A(m,n)",
            right:"m!/(m-n)!"
        },
        {
            left:"m!/(m-n)!",
            right:"A(m,n)"
        },
        {
            left:"P(n)",
            right:"n!"
        },
        {
            left:"n!",
            right:"P(n)"
        },
        {
            left:"a-(b-c)",
            right:"a-b+c"
        },
        {
            left:"a-b+c",
            right:"a-(b-c)"
        },
        {
            left:"a-b-c",
            right:"(a-b)-c"
        },
        {
            left:"(a-b)-c",
            right:"a-b-c"
        },
        {
            left:"a-a+b",
            right:"b"
        },
        {
            left: "a/b-c/b+d/b", 
            right: "(a-c+d)/b"
        },
        {
            left: "a/b+c/b+d/b", 
            right: "(a+c+d)/b"
        },
        {
            left: "a/b+c/b-d/b", 
            right: "(a+c-d)/b"},
        {
            left:"n!",
            right:"n*(n-1)!"},
        {
            left:"n*(n-1)!",
            right:"n!"},
        {
            left:"a/(b*c*d)+f/(c*d*e)",
            right:"(a*e+f*b)/(b*c*d*e)"
        },
        {
            left:"(a*e+f*b)/(b*c*d*e)",
            right:"a/(b*c*d)+f/(c*d*e)"
        },
        {
            left:"a*b/a*c",
            right:"b/c"
        },
        {
            left:"a+b-b",
            right:"a"
        },
        {
            left:"a-b+b",
            right:"a"
        }
    ],
    // [
    //     {
    //         left: "!(a&b)",
    //         right: "!a&!b"
    //     },
    //     {
    //         left: "!a&!b",
    //         right: "!(a&b)"
    //     }
    // ],
    [
        
    ]
);

const level5 = new Level(
    2,
    "",
    "C(n,k)*C(k,m)",
    "C(n,m)*C(n-m,k-m)",
    2,
    60,
    [
        {
            left:"C(m,(m-n))",
            right:"m!/((m-n)!(m-(m-n))!)"
        },
        {
            left:"m!/((m-n)!(m-(m-n))!)",
            right:"C(m,(m-n))"
        },
        {
            left:"C(m,n)",
            right:"m!/(n!(m-n)!)"
        },
        {
            left:"m!/(n!(m-n)!)",
            right:"C(m,n)"
        },
        {
            left:"A(m,n)",
            right:"m!/(m-n)!"
        },
        {
            left:"m!/(m-n)!",
            right:"A(m,n)"
        },
        {
            left:"P(n)",
            right:"n!"
        },
        {
            left:"n!",
            right:"P(n)"
        },
        {
            left:"a-(b-c)",
            right:"a-b+c"
        },
        {
            left:"a-b+c",
            right:"a-(b-c)"
        },
        {
            left:"a-b-c",
            right:"(a-b)-c"
        },
        {
            left:"(a-b)-c",
            right:"a-b-c"
        },
        {
            left:"a-a+b",
            right:"b"
        },
        {
            left: "a/b-c/b+d/b", 
            right: "(a-c+d)/b"
        },
        {
            left: "a/b+c/b+d/b", 
            right: "(a+c+d)/b"
        },
        {
            left: "a/b+c/b-d/b", 
            right: "(a+c-d)/b"},
        {
            left:"n!",
            right:"n*(n-1)!"},
        {
            left:"n*(n-1)!",
            right:"n!"},
        {
            left:"a/(b*c*d)+f/(c*d*e)",
            right:"(a*e+f*b)/(b*c*d*e)"
        },
        {
            left:"(a*e+f*b)/(b*c*d*e)",
            right:"a/(b*c*d)+f/(c*d*e)"
        },
        {
            left:"a*b/a*c",
            right:"b/c"
        },
        {
            left:"a+b-b",
            right:"a"
        },
        {
            left:"a-b+b",
            right:"a"
        }
    ],
    // [
    //     {
    //         left: "!(a&b)",
    //         right: "!a&!b"
    //     },
    //     {
    //         left: "!a&!b",
    //         right: "!(a&b)"
    //     }
    // ],
    [
        
    ]
);

const level6 = new Level(
    2,
    "",
    "A(m,n)",
    "A(m-1,n)+n*A(m-1,n-1)",
    2,
    60,
    [
        {
            left:"C(m,(m-n))",
            right:"m!/((m-n)!(m-(m-n))!)"
        },
        {
            left:"m!/((m-n)!(m-(m-n))!)",
            right:"C(m,(m-n))"
        },
        {
            left:"C(m,n)",
            right:"m!/(n!(m-n)!)"
        },
        {
            left:"m!/(n!(m-n)!)",
            right:"C(m,n)"
        },
        {
            left:"A(m,n)",
            right:"m!/(m-n)!"
        },
        {
            left:"m!/(m-n)!",
            right:"A(m,n)"
        },
        {
            left:"P(n)",
            right:"n!"
        },
        {
            left:"n!",
            right:"P(n)"
        },
        {
            left:"a-(b-c)",
            right:"a-b+c"
        },
        {
            left:"a-b+c",
            right:"a-(b-c)"
        },
        {
            left:"a-b-c",
            right:"(a-b)-c"
        },
        {
            left:"(a-b)-c",
            right:"a-b-c"
        },
        {
            left:"a-a+b",
            right:"b"
        },
        {
            left: "a/b-c/b+d/b", 
            right: "(a-c+d)/b"
        },
        {
            left: "a/b+c/b+d/b", 
            right: "(a+c+d)/b"
        },
        {
            left: "a/b+c/b-d/b", 
            right: "(a+c-d)/b"},
        {
            left:"n!",
            right:"n*(n-1)!"},
        {
            left:"n*(n-1)!",
            right:"n!"},
        {
            left:"a/(b*c*d)+f/(c*d*e)",
            right:"(a*e+f*b)/(b*c*d*e)"
        },
        {
            left:"(a*e+f*b)/(b*c*d*e)",
            right:"a/(b*c*d)+f/(c*d*e)"
        },
        {
            left:"a*b/a*c",
            right:"b/c"
        },
        {
            left:"a+b-b",
            right:"a"
        },
        {
            left:"a-b+b",
            right:"a"
        }
    ],
    // [
    //     {
    //         left: "!(a&b)",
    //         right: "!a&!b"
    //     },
    //     {
    //         left: "!a&!b",
    //         right: "!(a&b)"
    //     }
    // ],
    [
        
    ]
);

const level7 = new Level(
    2,
    "",
    "V(n,m)",
    "A(n+m,m)*m/(m+n)",
    2,
    60,
    [
        {
            left:"V(m,n)",
            right:"(m+n-1)!/((n-1)!)"
        },
        {
            left:"(m+n-1)!/((n-1)!)",
            right:"V(m,n)"
        },
        {
            left:"C(m,(m-n))",
            right:"m!/((m-n)!(m-(m-n))!)"
        },
        {
            left:"m!/((m-n)!(m-(m-n))!)",
            right:"C(m,(m-n))"
        },
        {
            left:"C(m,n)",
            right:"m!/(n!(m-n)!)"
        },
        {
            left:"m!/(n!(m-n)!)",
            right:"C(m,n)"
        },
        {
            left:"A(m,n)",
            right:"m!/(m-n)!"
        },
        {
            left:"m!/(m-n)!",
            right:"A(m,n)"
        },
        {
            left:"P(n)",
            right:"n!"
        },
        {
            left:"n!",
            right:"P(n)"
        },
        {
            left:"a-(b-c)",
            right:"a-b+c"
        },
        {
            left:"a-b+c",
            right:"a-(b-c)"
        },
        {
            left:"a-b-c",
            right:"(a-b)-c"
        },
        {
            left:"(a-b)-c",
            right:"a-b-c"
        },
        {
            left:"a-a+b",
            right:"b"
        },
        {
            left: "a/b-c/b+d/b", 
            right: "(a-c+d)/b"
        },
        {
            left: "a/b+c/b+d/b", 
            right: "(a+c+d)/b"
        },
        {
            left: "a/b+c/b-d/b", 
            right: "(a+c-d)/b"},
        {
            left:"n!",
            right:"n*(n-1)!"},
        {
            left:"n*(n-1)!",
            right:"n!"},
        {
            left:"a/(b*c*d)+f/(c*d*e)",
            right:"(a*e+f*b)/(b*c*d*e)"
        },
        {
            left:"(a*e+f*b)/(b*c*d*e)",
            right:"a/(b*c*d)+f/(c*d*e)"
        },
        {
            left:"a*b/a*c",
            right:"b/c"
        },
        {
            left:"a+b-b",
            right:"a"
        },
        {
            left:"a-b+b",
            right:"a"
        }
    ],
    // [
    //     {
    //         left: "!(a&b)",
    //         right: "!a&!b"
    //     },
    //     {
    //         left: "!a&!b",
    //         right: "!(a&b)"
    //     }
    // ],
    [
        
    ]
);

const level8 = new Level(
    2,
    "",
    "m*C(m-1,n-1)",
    "n*C(m,n)",
    2,
    60,
    [
        {
            left:"C(m,(m-n))",
            right:"m!/((m-n)!(m-(m-n))!)"
        },
        {
            left:"m!/((m-n)!(m-(m-n))!)",
            right:"C(m,(m-n))"
        },
        {
            left:"C(m,n)",
            right:"m!/(n!(m-n)!)"
        },
        {
            left:"m!/(n!(m-n)!)",
            right:"C(m,n)"
        },
        {
            left:"A(m,n)",
            right:"m!/(m-n)!"
        },
        {
            left:"m!/(m-n)!",
            right:"A(m,n)"
        },
        {
            left:"P(n)",
            right:"n!"
        },
        {
            left:"n!",
            right:"P(n)"
        },
        {
            left:"a-(b-c)",
            right:"a-b+c"
        },
        {
            left:"a-b+c",
            right:"a-(b-c)"
        },
        {
            left:"a-b-c",
            right:"(a-b)-c"
        },
        {
            left:"(a-b)-c",
            right:"a-b-c"
        },
        {
            left:"a-a+b",
            right:"b"
        },
        {
            left: "a/b-c/b+d/b", 
            right: "(a-c+d)/b"
        },
        {
            left: "a/b+c/b+d/b", 
            right: "(a+c+d)/b"
        },
        {
            left: "a/b+c/b-d/b", 
            right: "(a+c-d)/b"},
        {
            left:"n!",
            right:"n*(n-1)!"},
        {
            left:"n*(n-1)!",
            right:"n!"},
        {
            left:"a/(b*c*d)+f/(c*d*e)",
            right:"(a*e+f*b)/(b*c*d*e)"
        },
        {
            left:"(a*e+f*b)/(b*c*d*e)",
            right:"a/(b*c*d)+f/(c*d*e)"
        },
        {
            left:"a*b/a*c",
            right:"b/c"
        },
        {
            left:"a+b-b",
            right:"a"
        },
        {
            left:"a-b+b",
            right:"a"
        }
    ],
    // [
    //     {
    //         left: "!(a&b)",
    //         right: "!a&!b"
    //     },
    //     {
    //         left: "!a&!b",
    //         right: "!(a&b)"
    //     }
    // ],
    [
        
    ]
);

const level9 = new Level(
    2,
    "",
    "V(m,n)/A(k,k-m+1)",
    "A(m+n-1,m+n-1-k)/P(n)",
    2,
    60,
    [
        {
            left:"V(m,n)",
            right:"(m+n-1)!/((n-1)!)"
        },
        {
            left:"(m+n-1)!/((n-1)!)",
            right:"V(m,n)"
        },
        {
            left:"C(m,(m-n))",
            right:"m!/((m-n)!(m-(m-n))!)"
        },
        {
            left:"m!/((m-n)!(m-(m-n))!)",
            right:"C(m,(m-n))"
        },
        {
            left:"C(m,n)",
            right:"m!/(n!(m-n)!)"
        },
        {
            left:"m!/(n!(m-n)!)",
            right:"C(m,n)"
        },
        {
            left:"A(m,n)",
            right:"m!/(m-n)!"
        },
        {
            left:"m!/(m-n)!",
            right:"A(m,n)"
        },
        {
            left:"P(n)",
            right:"n!"
        },
        {
            left:"n!",
            right:"P(n)"
        },
        {
            left:"a-(b-c)",
            right:"a-b+c"
        },
        {
            left:"a-b+c",
            right:"a-(b-c)"
        },
        {
            left:"a-b-c",
            right:"(a-b)-c"
        },
        {
            left:"(a-b)-c",
            right:"a-b-c"
        },
        {
            left:"a-a+b",
            right:"b"
        },
        {
            left: "a/b-c/b+d/b", 
            right: "(a-c+d)/b"
        },
        {
            left: "a/b+c/b+d/b", 
            right: "(a+c+d)/b"
        },
        {
            left: "a/b+c/b-d/b", 
            right: "(a+c-d)/b"},
        {
            left:"n!",
            right:"n*(n-1)!"},
        {
            left:"n*(n-1)!",
            right:"n!"},
        {
            left:"a/(b*c*d)+f/(c*d*e)",
            right:"(a*e+f*b)/(b*c*d*e)"
        },
        {
            left:"(a*e+f*b)/(b*c*d*e)",
            right:"a/(b*c*d)+f/(c*d*e)"
        },
        {
            left:"a*b/a*c",
            right:"b/c"
        },
        {
            left:"a+b-b",
            right:"a"
        },
        {
            left:"a-b+b",
            right:"a"
        }
    ],
    // [
    //     {
    //         left: "!(a&b)",
    //         right: "!a&!b"
    //     },
    //     {
    //         left: "!a&!b",
    //         right: "!(a&b)"
    //     }
    // ],
    [
        
    ]
);

const level10 = new Level(
    2,
    "",
    "V(m,n)/A(k,k-m+1)",
    "A(m+n-1,m+n-1-k)/P(n)",
    2,
    60,
    [
        {
            left:"C(n)",
            right:"1/(n+1)*C(2n,n)"
        },
        {
            left:"1/(n+1)*C(2n,n)",
            right:"C(n)"
        },
        {
            left:"C(m,(m-n))",
            right:"m!/((m-n)!(m-(m-n))!)"
        },
        {
            left:"m!/((m-n)!(m-(m-n))!)",
            right:"C(m,(m-n))"
        },
        {
            left:"C(m,n)",
            right:"m!/(n!(m-n)!)"
        },
        {
            left:"m!/(n!(m-n)!)",
            right:"C(m,n)"
        },
        {
            left:"A(m,n)",
            right:"m!/(m-n)!"
        },
        {
            left:"m!/(m-n)!",
            right:"A(m,n)"
        },
        {
            left:"P(n)",
            right:"n!"
        },
        {
            left:"n!",
            right:"P(n)"
        },
        {
            left:"a-(b-c)",
            right:"a-b+c"
        },
        {
            left:"a-b+c",
            right:"a-(b-c)"
        },
        {
            left:"a-b-c",
            right:"(a-b)-c"
        },
        {
            left:"(a-b)-c",
            right:"a-b-c"
        },
        {
            left:"a-a+b",
            right:"b"
        },
        {
            left: "a/b-c/b+d/b", 
            right: "(a-c+d)/b"
        },
        {
            left: "a/b+c/b+d/b", 
            right: "(a+c+d)/b"
        },
        {
            left: "a/b+c/b-d/b", 
            right: "(a+c-d)/b"},
        {
            left:"n!",
            right:"n*(n-1)!"},
        {
            left:"n*(n-1)!",
            right:"n!"},
        {
            left:"a/(b*c*d)+f/(c*d*e)",
            right:"(a*e+f*b)/(b*c*d*e)"
        },
        {
            left:"(a*e+f*b)/(b*c*d*e)",
            right:"a/(b*c*d)+f/(c*d*e)"
        },
        {
            left:"a*b/a*c",
            right:"b/c"
        },
        {
            left:"a+b-b",
            right:"a"
        },
        {
            left:"a-b+b",
            right:"a"
        }
    ],
    // [
    //     {
    //         left: "!(a&b)",
    //         right: "!a&!b"
    //     },
    //     {
    //         left: "!a&!b",
    //         right: "!(a&b)"
    //     }
    // ],
    [
        
    ]
);

const level11 = new Level(
    2,
    "",
    "C(2n,n)-C(2n,n-1)",
    "1/(n+1)*C(2n,n)",
    2,
    60,
    [
        {
            left:"C(m,(m-n))",
            right:"m!/((m-n)!(m-(m-n))!)"
        },
        {
            left:"m!/((m-n)!(m-(m-n))!)",
            right:"C(m,(m-n))"
        },
        {
            left:"C(m,n)",
            right:"m!/(n!(m-n)!)"
        },
        {
            left:"m!/(n!(m-n)!)",
            right:"C(m,n)"
        },
        {
            left:"A(m,n)",
            right:"m!/(m-n)!"
        },
        {
            left:"m!/(m-n)!",
            right:"A(m,n)"
        },
        {
            left:"P(n)",
            right:"n!"
        },
        {
            left:"n!",
            right:"P(n)"
        },
        {
            left:"a-(b-c)",
            right:"a-b+c"
        },
        {
            left:"a-b+c",
            right:"a-(b-c)"
        },
        {
            left:"a-b-c",
            right:"(a-b)-c"
        },
        {
            left:"(a-b)-c",
            right:"a-b-c"
        },
        {
            left:"a-a+b",
            right:"b"
        },
        {
            left: "a/b-c/b+d/b", 
            right: "(a-c+d)/b"
        },
        {
            left: "a/b+c/b+d/b", 
            right: "(a+c+d)/b"
        },
        {
            left: "a/b+c/b-d/b", 
            right: "(a+c-d)/b"},
        {
            left:"n!",
            right:"n*(n-1)!"},
        {
            left:"n*(n-1)!",
            right:"n!"},
        {
            left:"a/(b*c*d)+f/(c*d*e)",
            right:"(a*e+f*b)/(b*c*d*e)"
        },
        {
            left:"(a*e+f*b)/(b*c*d*e)",
            right:"a/(b*c*d)+f/(c*d*e)"
        },
        {
            left:"a*b/a*c",
            right:"b/c"
        },
        {
            left:"a+b-b",
            right:"a"
        },
        {
            left:"a-b+b",
            right:"a"
        }
    ],
    // [
    //     {
    //         left: "!(a&b)",
    //         right: "!a&!b"
    //     },
    //     {
    //         left: "!a&!b",
    //         right: "!(a&b)"
    //     }
    // ],
    [
        
    ]
);

//attendantRules.forEach(rule => addRuleToCollectionIfNotExists(rule, Level.attendantRules));